import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'v-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  selectedNavlink: string = '';

  constructor(public commonService: CommonService, public router: Router) { }

  ngOnInit(): void {
  
  }

  goto(id: string) {
    this.router.navigate(['/']);
    this.selectedNavlink = id;
    setTimeout(() => {
      this.commonService.setScrollTo(id);
    }, 200)

  }


}
