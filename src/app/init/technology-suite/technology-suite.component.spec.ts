import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologySuiteComponent } from './technology-suite.component';

describe('TechnologySuiteComponent', () => {
  let component: TechnologySuiteComponent;
  let fixture: ComponentFixture<TechnologySuiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnologySuiteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TechnologySuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
