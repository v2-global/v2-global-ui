import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitComponent } from './init.component';
import { InitContentComponent } from './init-content/init-content.component';
import { CareersComponent } from './careers/careers.component';

const routes: Routes = [
  {
    path: '', component: InitComponent,
    children: [
      { path: '', component: InitContentComponent },
      { path: 'careers', component: CareersComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitRoutingModule { }
