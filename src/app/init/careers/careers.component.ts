import { Component } from '@angular/core';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent {

  careerJobs =[
    {
      title:'Job Title',
      description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus nam, facere, minima dolorem tenetur aliquid architecto eaque alias ex pariatur quas est numquam possimus adipisci eveniet necessitatibus, unde commodi neque?',
      requirements:[
        'HTML', 'CSS', 'Angular', 'Nodejs'
      ]
    },
    {
      title:'Job Title',
      description:'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatibus nam, facere, minima dolorem tenetur aliquid architecto eaque alias ex pariatur quas est numquam possimus adipisci eveniet necessitatibus, unde commodi neque?',
      requirements:[
        'HTML', 'CSS', 'Angular', 'Reactjs'
      ]
    },
  ]


}
