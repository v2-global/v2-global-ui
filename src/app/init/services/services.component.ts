import { Component } from '@angular/core';

@Component({
  selector: 'v-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {

 myServices = [
  { title:'Banking and Finance', image:'assets/images/icons/banking_finance.png' },
  { title:'Manufacturing', image:'assets/images/icons/manufacturing.png' },
  { title:'Professional Services', image:'assets/images/icons/professional_services.png' },
  { title:'Telecom', image:'assets/images/icons/telecom.png' },
  { title:'Healthcare & Insurance', image:'assets/images/icons/healthcare.png' },
  { title:'Airline', image:'assets/images/icons/airline.png' },
  { title:'Automobile', image:'assets/images/icons/automobile.png' },
  { title:'BPO', image:'assets/images/icons/bpo.png' },
  { title:'Education', image:'assets/images/icons/education.png' },
  { title:'Hospitality', image:'assets/images/icons/hospitality.png' },
  { title:'Retail', image:'assets/images/icons/retail.png' },
  { title:'Telecommunication', image:'assets/images/icons/telecommunications.png' },
  { title:'Product Engineering', image:'assets/images/icons/product_engineering.png' },
  { title:'Managed Services', image:'assets/images/icons/managed_services.png' },
  { title:'Quality Assurance', image:'assets/images/icons/quality_assurance.png' },
 ]


}
