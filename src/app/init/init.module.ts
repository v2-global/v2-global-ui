import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitRoutingModule } from './init-routing.module';
import { InitComponent } from './init.component';
import { MaterialModule } from 'src/vendors/material/material.module';
import { NavbarComponent } from './navbar/navbar.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ServicesComponent } from './services/services.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CommonService } from '../providers/common.service';
import { CareersComponent } from './careers/careers.component';
import { InitContentComponent } from './init-content/init-content.component';
import { TechnologySuiteComponent } from './technology-suite/technology-suite.component';

@NgModule({
  declarations: [
    InitComponent,
    NavbarComponent,
    AboutUsComponent,
    ContactUsComponent,
    ServicesComponent,
    HeaderComponent,
    FooterComponent,
    CareersComponent,
    InitContentComponent,
    TechnologySuiteComponent,
  ],
  imports: [
    CommonModule,
    InitRoutingModule,
    MaterialModule
  ],
  providers:[
    CommonService
  ]
})
export class InitModule { }
