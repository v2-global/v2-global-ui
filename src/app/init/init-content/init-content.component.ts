import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'v-init-content',
  templateUrl: './init-content.component.html',
  styleUrls: ['./init-content.component.scss']
})
export class InitContentComponent implements OnInit {

  constructor(public commonService: CommonService) { }

  ngOnInit(): void {

    this.commonService.getScrollTo().subscribe(res => {
      if (res) {
        const el = document.getElementById(res) as HTMLElement;
        if (el) {
          el.style.scrollMargin = '55px'
          el.scrollIntoView({ behavior: 'smooth', block:'start' });
        }
      }
    })
  }

}
