import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'v-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{

  selectedNavlink: string = '';

  constructor(public commonService: CommonService, public router: Router) { }

  ngOnInit(): void { 
    if(this.router.url === '/'){
      this.selectedNavlink = 'home';
    }
  }

  goto(id: string) {
    this.router.navigate(['/']);
    this.selectedNavlink = id;
    setTimeout(()=>{
      this.commonService.setScrollTo(id);
    }, 200)
    
  }


}
