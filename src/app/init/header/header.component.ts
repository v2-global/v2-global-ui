import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'v-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public commonService: CommonService) { }

  ngOnInit(): void {
    this.commonService.getScrollTo().subscribe(res => {
      if (res) {
        const el = document.getElementById(res) as HTMLElement;
        if (el) {
          el.style.scrollMargin = '55px'
          el.scrollIntoView({ behavior: 'smooth', block:'start' });
        }
      }
    })
  }

}
