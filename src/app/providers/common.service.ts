import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public scrollTo: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() { }

  setScrollTo(id: string) {
    this.scrollTo.next(id);
  }
  getScrollTo() {
    return this.scrollTo.asObservable();
  }
  

}
